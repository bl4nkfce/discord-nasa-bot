const { setNasaApiKey } = require('nasa-sdk');

setNasaApiKey(process.env.NASA_API_KEY);