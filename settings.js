const moment = require("moment");
const client = require("./index");
const util = require("./util");
const defaultEnabledCommands = ["help", "commands", "info", "apod"];

function Settings(settingsObj = {}) {
  this.scheduledJobs = settingsObj.scheduledJobs || [];
  this.scheduledJobsEnabled = settingsObj.scheduledJobsEnabled || false;
  this.enabledCommands = settingsObj.enabledCommands || [
    ...defaultEnabledCommands
  ];
  this.botModerators = settingsObj.botModerators || [];
  this.allowedChannels = settingsObj.allowedChannels || [{ name: "mention" }];
}

module.exports = {
  Settings,
  defaultEnabledCommands
}