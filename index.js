// set the api keys and stuff
require("dotenv").config();
const fs = require("fs");
const path = require("path");
const express = require("express");
const app = express();
const port = process.env.PORT || 3000;
app.use(express.static(path.join(__dirname, "public")));
app.get("*", (req, res) => {
  res.redirect("/");
});
app.listen(port, () => {
  console.log("Express listening on port:", port);
  // set nasa api key
  require("./nasa");
  const util = require("./util");
  const Discord = require("discord.js");
  const client = new Discord.Client();

  client.settings = new Discord.Collection();

  // each command is a separate module
  client.commands = new Discord.Collection();
  const commandFiles = fs
    .readdirSync("./commands")
    .filter(file => file.endsWith(".js") && !file.includes('init'));

  // require all the commands
  for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
  }

  client.once("ready", () => {
    console.log("connected");
    console.log("loaded commands: ", client.commands.keyArray());
  });

  client.on("message", message => {
    if (util.isBotCommand(message)) {
      util.handleCommand(client, message);
    }
  });

  client.login(process.env.BOT_AUTH_TOKEN).catch(error => {
    console.error(error);
  });
  module.exports = client;
});