/**
 * set up basic bot settings
 * TODO: store settings in db and fetch/save when needed
 */
const util = require('../util');
const Settings = require('../settings');
const prefix = process.env.BOT_PREFIX;
module.exports = {
  name: "init",
  description: "Initializes the bot with the basic settings",
  example: `${prefix} init`,
  execute: (client, args, message) => {
    client.settings.set(message.guild.id, new Settings());
    return util.resolveWithResult({
      messages: [
        'Successfully initialized bot configuration'
      ]
    });
  }
};
