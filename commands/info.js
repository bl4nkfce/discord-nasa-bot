const prefix = process.env.BOT_PREFIX;
const util = require("../util");
const exampleUsages = [
  `${prefix}`,
  `${prefix} info`
];
module.exports = {
  name: "info",
  description: "Displays information about the bot",
  example: exampleUsages.join('\`, \`'),
  execute: () => {
    return util.resolveWithResult({
      messages: [
        `nasa-bot v${process.env.BOT_VERSION}`,
        `Type \`${prefix} commands\` to see available commands.`,
        `Type \`${prefix} <command>\` to use a command.`
      ]
    });
  }
};
