const prefix = process.env.BOT_PREFIX;
const util = require("../util");
module.exports = {
  name: "help",
  description: "Displays help information about specified command",
  example: `\`${process.env.BOT_PREFIX} help apod\``,
  execute: (client, args) => {
    const messages = [];
    const commandName = args[0];
    if (!commandName) {
      commandName = "info";
    }
    const command = client.commands.get(commandName);
    if (command) {
      messages.push(
        `**Name:** ${command.name}`,
        `**Description**: ${command.description}`,
        `**Example usage**: \`${command.example}\``
      );
    } else {
      messages.push(
        `Command ${commandName} is not supported.`,
        `Type \`${prefix} commands\` to see the list of available commands.`
      );
    }
    return util.resolveWithResult({
      messages: messages
    });
  }
};
