const Settings = require("../settings").Settings;
const util = require("../util");
const prefix = process.env.BOT_PREFIX;
const description = `Changes the bot configuration.`;

const errorMessages = {
  configNotFound: () => `Unable to get bot configuration. Try running \`!nasa init\` and try again.`,
  unknownError: "Unable to update bot configuration.",
  actionNotSpecified: (optionToUpdate) => `${optionToUpdate.name} value missing. Available values: ${optionToUpdate.actions.join(', ')}`
};

const options = [
  { name: "enabledCommands", actions: ["add", "remove"], enabled: false },
  { name: "allowedChannels", actions: ["add", "remove"], enabled: false },
  { name: "botModerators", actions: ["add", "remove"], enabled: false },
  {
    name: "scheduledJobsEnabled",
    actions: ["toggle", "off", "on", "true", "false"],
    enabled: true
  }
];

function getUpdatedConfig(oldConfig, option, action) {
  const updatedConfig = new Settings(oldConfig);
  if (option === "scheduledJobsEnabled") {
    if (action === "toggle") {
      updatedConfig[option] = !updatedConfig[action];
    } else if (action === "on" || action === "true") {
      updatedConfig[option] = true;
    } else if (action === "off" || action === "false") {
      updatedConfig[option] = false;
    }
  }
  return updatedConfig;
}

module.exports = {
  name: "config",
  description: description,
  example: `\`${prefix} config scheduledJobsEnabled false\``,
  execute: (client, args, message) => {
    const messages = [];
    const [option, optionAction] = args;
    const configToUpdate = client.settings.get(message.guild.id);
    if (!configToUpdate) {
      messages.push(errorMessages.configNotFound()); 
    } else if (configToUpdate && !option) {
      messages.push(`\`\`\`${JSON.stringify(configToUpdate, null, 2)}\`\`\``);
    } else if (configToUpdate && option) {
      const optionToUpdate = options.find(o => o.name === option && o.enabled);
      if (optionToUpdate && optionAction) {
        try {
          client.settings.set(
            message.guild.id,
            getUpdatedConfig(configToUpdate, option, optionAction)
          );
          messages.push(
            "Successfully updated bot configuration. To see the bot configuration use `!nasa config`"
          );
        } catch (e) {
          console.error(e);
          messages.push(errorMessages.unknownError);
        }
      } else if (optionToUpdate && !optionAction) {
        messages.push(errorMessages.actionNotSpecified)
      }
       else {
        messages.push(errorMessages.unknownError);
      }
    }
    return util.resolveWithResult({
      messages
    });
  }
};
