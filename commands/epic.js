const prefix = process.env.BOT_PREFIX;
const util = require("../util");
const { EPIC } = require("nasa-sdk");
const moment = require("moment");
const dateFormat = "YYYY-MM-DD";
const apiDateFormat = "YYYY/MM/DD";
const defaultType = "natural";
const EPICImageURL = "https://epic.gsfc.nasa.gov/archive";
const allowedTypes = ["natural", "enhanced"];

const errorMessages = {
  invalidDateFormat: [
    `Invalid date format. Use \`${prefix} epic YYYY-MM-DD\` instead.`,
    `Example: \`${prefix} epic 2019-05-10\`.`
  ],
  dateTooBig: `EPIC date cannot be greater that today.`,
  dateTooSmall: `EPIC date must be grater than June 16 1995`,
  typeInvalid: `Specified type is invalid. Allowed values: ${allowedTypes.join(
    ", "
  )}`,
  epicUnavailable: date =>
    `EPIC for ${date} is unavailable at this moment. Please try again later.`
};

function isDateFormatValid(date) {
  return moment(date, dateFormat, true).isValid();
}

function isDateTooBig(date) {
  return (
    isDateFormatValid(date) &&
    moment(date).format(dateFormat) > moment().format(dateFormat)
  );
}

function isDateTooSmall(date) {
  return (
    isDateFormatValid(date) &&
    moment(date).format(dateFormat) < moment("1995-06-16").format(dateFormat)
  );
}

function isTypeInvalid(type) {
  return !allowedTypes.includes(type);
}

function getEPICLinks(epics, type, date) {
  const formattedDate = moment(date).format(apiDateFormat);
  return epics.map(
    epic =>
      `${EPICImageURL}/${type}/${formattedDate}/png/${epic.image}.png?api_key=${
        process.env.NASA_API_KEY
      }`
  );
}

function getEPICAttachment(links, date) {
  return util.createGIF(links, `epic-${date.replace(/\-/g, "")}`);
}

function getEPIC(client, args) {
  let date = args[0];
  const type = args[1] || defaultType;
  if (!date || date === "today") {
    date = moment().format(dateFormat);
  }
  if (date && !isDateFormatValid(date)) {
    return util.resolveWithResult({
      messages: errorMessages.invalidDateFormat
    });
  } else if (date && isDateTooBig(date)) {
    return util.resolveWithResult({
      messages: [errorMessages.dateTooBig]
    });
  } else if (date && isDateTooSmall(date)) {
    return util.resolveWithResult({
      messages: [errorMessages.dateTooSmall]
    });
  } else if (isTypeInvalid(type)) {
    return util.resolveWithResult({
      messages: [errorMessages.typeInvalid]
    });
  } else {
    return EPIC.date(type, date)
      .then(epics => {
        const result = { messages: [], options: {} };
        return getEPICAttachment(getEPICLinks(epics, type, date), date).then(
          image => {
            result.messages.push(epics[0].caption || "test stuff");
            result.options.files = [{
              attachment: image,
              name: `epic-${date.replace(/\-/, '')}.gif`
            }];
            return util.resolveWithResult(result);
          }
        );
      })
      .catch(error => {
        console.error(error);
        const statusCode = error
          .toString()
          .split("statusCode=")
          .pop();
        if (statusCode === "500") {
          return util.resolveWithResult({
            messages: [errorMessages.epicUnavailable(date)]
          });
        }
      });
  }
}

module.exports = {
  name: "epic",
  description:
    "Returns a GIF combined of imagery collected by DSCOVR's Earth Polychromatic Imaging Camera (EPIC) instrument.",
  example: `${prefix} epic 2019-05-10`,
  execute: getEPIC
};
