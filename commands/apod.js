const prefix = process.env.BOT_PREFIX;
const util = require("../util");
const { APOD } = require("nasa-sdk");
const moment = require("moment");
const dateFormat = "YYYY-MM-DD";
const officialAPODLinkDateFormat = "YY-MM-DD";

const errorMessages = {
  invalidDateFormat: [
    `Invalid date format. Use \`${prefix} apod YYYY-MM-DD\` instead.`,
    `Example: \`${prefix} apod 2018-04-20\`.`
  ],
  dateTooBig: `APOD date cannot be greater that today.`,
  dateTooSmall: `APOD date must be grater than June 16 1995`,
  apodUnavailable: (date) => `APOD for ${date} is unavailable at this moment. Please try again later.`
};

function isDateFormatValid(date) {
  return moment(date, dateFormat, true).isValid();
}

function isDateTooBig(date) {
  return (
    isDateFormatValid(date) &&
    moment(date).format(dateFormat) > moment().format(dateFormat)
  );
}

function isDateTooSmall(date) {
  return (
    isDateFormatValid(date) &&
    moment(date).format(dateFormat) < moment("1995-06-16").format(dateFormat)
  );
}

function officialAPODLink(date) {
  const dateValid =
    isDateFormatValid(date) && !isDateTooBig(date) && !isDateTooSmall(date);
  if (dateValid) {
    const d = moment(date)
      .format(officialAPODLinkDateFormat)
      .replace(/\-/g, "");
    return `https://apod.nasa.gov/apod/ap${d}.html`;
  } else return "";
}

function getVideoAPODMessage(apod) {
  return [
    `**${apod.title}** *(${apod.date})*`,
    apod.copyright ? `*Copyright: ${apod.copyright}*` : "",
    `*More Info: ${apod.officialLink}*`,
    `*${apod.url}*`
  ];
}

function getAPODEmbed(apod) {
  return {
    author: {
      name: apod.copyright
    },
    description: apod.explanation,
    color: 0x000000,
    fields: [
      {
        name: "More info",
        value: officialAPODLink(apod.date),
        inline: true
      }
    ],
    image: {
      url: apod.hdurl || apod.url
    },
    timestamp: apod.date,
    title: apod.title,
    url: apod.hdurl || apod.url
  };
}

function getAPOD(client, args) {
  let date = args[0];
  if (!date || date === "today") {
    date = moment().format(dateFormat);
  }
  if (date && !isDateFormatValid(date)) {
    return util.resolveWithResult({
      messages: errorMessages.invalidDateFormat
    });
  } else if (date && isDateTooBig(date)) {
    return util.resolveWithResult({
      messages: [errorMessages.dateTooBig]
    });
  } else if (date && isDateTooSmall(date)) {
    return util.resolveWithResult({
      messages: [errorMessages.dateTooSmall]
    });
  } else {
    return APOD.fetch({
      date: date,
      hd: true
    })
      .then(apod => {
        const result = { messages: [], options: {} };
        apod.officialLink = officialAPODLink(date);
        if (apod.media_type === "video") {
          apod.url = apod.url.replace("embed/", "watch?v=");
          result.messages.push(...getVideoAPODMessage(apod));
        } else {
          result.options.embed = getAPODEmbed(apod);
        }
        return util.resolveWithResult(result);
      })
      .catch(error => {
        console.error(error);
        const statusCode = error.toString().split('statusCode=').pop();
        if (statusCode === '500') {
          return util.resolveWithResult({
            messages: [errorMessages.apodUnavailable(date)]
          });
        }
      });
  }
}

module.exports = {
  name: "apod",
  description: "Displays Astronomy Picture Of the Day for a specified date",
  example: `${prefix} apod 2018-12-31`,
  execute: getAPOD
};
