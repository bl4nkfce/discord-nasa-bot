const util = require("../util");
module.exports = {
  name: "commands",
  description: "Lists all available commands",
  example: `\`${process.env.BOT_PREFIX} commands\``,
  execute: client => {
    return util.resolveWithResult({
      messages: [`Available commands: ${client.commands.keyArray().join(", ")}`]
    });
  }
};
