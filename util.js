function isBotCommand(message) {
  return message.content.startsWith(process.env.BOT_PREFIX);
}

async function handleCommand(client, message) {
  const [commandName, ...args] = message.content
    .split(" ")
    .slice(1)
    .filter(x => x);

  try {
    const commandResult = await callCommand(client, commandName, args, message);
    if (commandResult) {
      let {messages, options} = commandResult;
      if (options) {
        options.split = true;
      } else {
        options = {
          split: true
        };
      }
      // do some logging here
      return message.channel.send(messages.join("\n"), options);
    }
  } catch (err) {
    // do some logging here
    console.error(err);
    return;
  }
}

function callCommand(client, commandName, args, message) {
  let command = client.commands.get(commandName);
  if (!command) {
    command = client.commands.get("info");
  }
  return command.execute(client, args, message);
}

function resolveWithResult(commandResult) {
  return Promise.resolve(commandResult);
}

function createGIF(imageURLs, fileName) {
  const { loadImage, createCanvas } = require("canvas"),
    GIFEncoder = require("gifencoder"),
    fs = require("fs"),
    path = require("path");
  const images = imageURLs.map(url => {
    console.log("loading image");
    return loadImage(url);
  });

  return Promise.all(images)
    .then(images => {
      console.log("loaded images, creating a gif now :)");
      const encoder = new GIFEncoder(512, 512),
        canvas = createCanvas(512, 512),
        ctx = canvas.getContext("2d");

      encoder.start();
      encoder.setRepeat(0);
      encoder.setDelay(0);
      encoder.setQuality(5);
      images.forEach(img => {
        ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, 512, 512);
        encoder.addFrame(ctx);
      });
      encoder.finish();
      // add some stuff for file storage
      return encoder.out.getData();
    })
    .catch(error => {
      console.error(error);
    });
}

module.exports = {
  isBotCommand,
  handleCommand,
  resolveWithResult,
  callCommand,
  createGIF
};
